#!/usr/bin/python3

import builtins
import os
import sys
import re
import getopt
import subprocess
from time import sleep

testOnly = False


class Position:
    def __init__(self, x, y, width, height, keys=""):
        self.gravity = 0
        self.keys = keys
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def log(self):
        return f"x={self.x}, y={self.y}, w={self.width}, h={self.height}"

    ## wmctrl format: gravity,posx,posy,width,height
    def __repr__(self):
        return f"{self.gravity},{self.x},{self.y},{self.width},{self.height}"


def runCommand(command):
    if testOnly:
        print(command)
    else:
        os.system(command)


def runWithOutput(command):
    if testOnly:
        print(command)

    out = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        shell=True,
        text=True,
    )
    return str(out.stdout).strip()


fullScreen, screenSize = runWithOutput("wmctrl -d | head -n 1 | grep -Eho '[0-9]{4}x[0-9]{3,4}'").split("\n")
MAX_W, MAX_H = [int(x) for x in screenSize.split("x")]
FULLSCREEN_W, FULLSCREEN_H = [int(x) for x in fullScreen.split("x")]
PANEL_H = FULLSCREEN_H - MAX_H
PANEL_W = FULLSCREEN_W - MAX_W
HALF_W = int(MAX_W / 2)
HALF_H = int(MAX_H / 2)
THIRD_W = int(MAX_W / 3)
THIRD_H = int(MAX_H / 3)
QUARTER_W = int(MAX_W / 4)
QUARTER_H = int(MAX_H / 4)
EXTRA_PIX_W = MAX_W % 2
EXTRA_PIX_H = MAX_H % 2
print(f"Display resolution: {fullScreen}, width={FULLSCREEN_W}, height={FULLSCREEN_H}")
print(f"Screen size: {screenSize}, width={MAX_W}, height={MAX_H}")
print(f"Extra pixel correction: width={EXTRA_PIX_W}, height={EXTRA_PIX_H}")
print(f"half: width={HALF_W}, height={HALF_H}")
print(f"third: width={THIRD_W}, height={THIRD_H}")
print(f"quarter: width={QUARTER_W}, height={QUARTER_H}")

snapPositions = {
    "left": Position(0, 0, HALF_W + EXTRA_PIX_W, MAX_H, "Super+Left"),
    "right": Position(HALF_W, 0, HALF_W, MAX_H, "Super+Right"),
    "top": Position(0, 0, MAX_W, HALF_H + EXTRA_PIX_H, "Super+Up"),
    "bottom": Position(0, HALF_H, MAX_W, HALF_H, "Super+Down"),
    "top-left": Position(0, 0, HALF_W + EXTRA_PIX_W, HALF_H + EXTRA_PIX_H, "Super+Up+Left"),
    "top-right": Position(HALF_W, 0, HALF_W, HALF_H + EXTRA_PIX_H, "Super+Up+Right"),
    "bottom-left": Position(0, HALF_H, HALF_W + EXTRA_PIX_W, HALF_H, "Super+Down+Left"),
    "bottom-right": Position(HALF_W, HALF_H, HALF_W, HALF_H, "Super+Down+Right"),

    "2column1": Position(0, 0, HALF_W + EXTRA_PIX_W, MAX_H, "Super+Left"),
    "2column2": Position(HALF_W, 0, HALF_W, MAX_H, "Super+Right"),

    "3column1": Position(0, 0, THIRD_W, MAX_H),
    "3column2": Position(THIRD_W, 0, THIRD_W, MAX_H),
    "3column3": Position(THIRD_W * 2, 0, THIRD_W, MAX_H),

    "3column1and2": Position(0, 0, THIRD_W * 2, MAX_H),
    "3column2and3": Position(THIRD_W, 0, THIRD_W * 2, MAX_H),

    "3column1splitTop": Position(0, 0, THIRD_W, HALF_H + EXTRA_PIX_H),
    "3column1splitBottom": Position(0, HALF_H, THIRD_W, HALF_H),
    "3column2splitTop": Position(THIRD_W, 0, THIRD_W, HALF_H),
    "3column2splitBottom": Position(THIRD_W, HALF_H, THIRD_W, HALF_H),
    "3column3splitTop": Position(THIRD_W * 2, 0, THIRD_W, HALF_H + EXTRA_PIX_H),
    "3column3splitBottom": Position(THIRD_W * 2, HALF_H, THIRD_W, HALF_H),

    "4column1": Position(0, 0, QUARTER_W, MAX_H),
    "4column2": Position(QUARTER_W, 0, QUARTER_W, MAX_H),
    "4column3": Position(HALF_W, 0, QUARTER_W, MAX_H),
    "4column4": Position(QUARTER_W * 3, 0, QUARTER_W, MAX_H),

    "4column1and2": Position(0, 0, HALF_W + EXTRA_PIX_W, MAX_H, "Super+Left"),
    "4column2and3": Position(QUARTER_W, 0, HALF_W, MAX_H),
    "4column3and4": Position(HALF_W, 0, HALF_W, MAX_H, "Super+Right"),

    "4column1splitTop": Position(0, 0, QUARTER_W, HALF_H + EXTRA_PIX_H),
    "4column1splitBottom": Position(0, HALF_H, QUARTER_W, HALF_H),
    "4column2splitTop": Position(QUARTER_W, 0, QUARTER_W, HALF_H + EXTRA_PIX_H),
    "4column2splitBottom": Position(QUARTER_W, HALF_H, QUARTER_W, HALF_H),
    "4column3splitTop": Position(QUARTER_W * 2, 0, QUARTER_W, HALF_H + EXTRA_PIX_H),
    "4column3splitBottom": Position(QUARTER_W * 2, HALF_H, QUARTER_W, HALF_H),
    "4column4splitTop": Position(QUARTER_W * 3, 0, QUARTER_W, HALF_H + EXTRA_PIX_H),
    "4column4splitBottom": Position(QUARTER_W * 3, HALF_H, QUARTER_W, HALF_H),

}
# x, y, width, height


def moveWindow(title, type=None, desktop=0):
    wid = __getPID(title, type)
    for pid in wid:
        deskMsg = __moveToDesktop(pid, desktop, False)
        print(f"Moving '{title}', pid: {pid}, {deskMsg}")


def snapWindow(title, positionName, desktop=0, type=None, stayOnTop=False, simulateSnap=False, sticky=False, xCorrection=0, yCorrection=0):
    print("--")
    wid = __getPID(title, type)
    for pid in wid:
        position = snapPositions.get(positionName)
        arrowKeys = position.keys
        if simulateSnap == True or arrowKeys == "":
            pos = __buildPositionWithExtents(pid, snapPositions.get(positionName))
            __fakeSnap(title, positionName, pid, desktop, pos, stayOnTop, sticky, xCorrection, yCorrection)
        else:
            __realSnap(title, positionName, pid, desktop, arrowKeys, stayOnTop, sticky)

        __displayFinalPosition(pid)


def __fakeSnap(title, positionName, pid, desktop, position, stayOnTop, sticky, xCorrection, yCorrection):
    deskMsg = __moveToDesktop(pid, desktop, sticky)
    stickyMsg = __setSticky(pid, sticky)
    onTopMsg = __stayOnTop(pid, stayOnTop)

    runCommand(f"wmctrl -ir {pid} -b remove,shaded")
    runCommand(f"wmctrl -ir {pid} -b remove,maximized_vert,maximized_horz")

    __setPosition(pid, position, xCorrection, yCorrection)
    print(
        f"Imitating snap for '{title}', pid: {pid}, to '{positionName}' {deskMsg}at position {position.log()} {stickyMsg}{onTopMsg}"
    )


def __realSnap(title, positionName, pid, desktop, arrowKeys, stayOnTop, sticky):
    #
    # Switch to the correct desktop, then move the window to the active desktop
    # Then we can run the key commands for snapping
    #
    runCommand(f"wmctrl -s {desktop}")
    sleep(0.2)
    runCommand(f"wmctrl -iR {pid}")
    runCommand(f'xdotool key "{arrowKeys}"')
    sleep(0.1)
    stickyMsg = __setSticky(pid, sticky)
    onTopMsg = __stayOnTop(pid, stayOnTop)
    print(
        f"Snapping window '{title}', pid: {pid}, to {positionName} with {arrowKeys} on desktop '{desktop}' {stickyMsg}{onTopMsg}"
    )


def maximizeWindow(title, desktop, type=None, stayOnTop=False):
    print("--")
    wid = __getPID(title, type)
    for pid in wid:
        deskMsg = __moveToDesktop(pid, desktop, False)
        runCommand(f"wmctrl -ir {pid} -b remove,shaded")
        runCommand(f"wmctrl -ir {pid} -b add,maximized_vert,maximized_horz")
        onTopMsg = __stayOnTop(pid, stayOnTop)
        print(f"Maximizing '{title}', pid: {pid}, {deskMsg}{onTopMsg}")
        __displayFinalPosition(pid)


def repositionWindow(title, x, y, w, h, desktop=0, type=None, stayOnTop=False, sticky=False):
    print("--")
    wid = __getPID(title, type)
    for pid in wid:
        position = __buildPositionWithExtents(pid, Position(x, y, w, h))

        deskMsg = __moveToDesktop(pid, desktop, sticky)
        stickyMsg = __setSticky(pid, sticky)
        __setPosition(pid, position)
        onTopMsg = __stayOnTop(pid, stayOnTop)
        runCommand(f"wmctrl -a '{title}'")

        print(f"Repositioning '{title}', pid: {pid}, {deskMsg}at position {position.log()} {stickyMsg}{onTopMsg}")
        __displayFinalPosition(pid)


def __displayFinalPosition(pid):
    msg = runWithOutput(f"wmctrl -lpGx | grep {pid}")
    msg = re.sub("\s+", " ", msg).split(" ")
    x, y, w, h = msg[3], msg[4], msg[5], msg[6]
    print(f"Position reported by wmctrl: x={x}, y={y}, w={w}, h={h}")


def __setSticky(pid, sticky):
    msg = ""
    if sticky:
        runCommand(f"wmctrl -ir {pid} -b add,sticky")
        msg = "making window sticky "
    return msg


def __setPosition(pid, position, xCorrection=0, yCorrection=0):
    if xCorrection == 0 and yCorrection == 0:
        np = position
    else:
        np = Position(position.x + xCorrection, position.y + yCorrection, position.width, position.height, position.keys)

    runCommand(f"wmctrl -ir {pid} -e {np}")


def __moveToDesktop(pid, desktop, sticky):
    msg = ""
    if not sticky:
        msg = f"on desktop '{desktop}' "
        runCommand(f"wmctrl -ir {pid} -t {desktop}")
    return msg


def __stayOnTop(pid, stayOnTop):
    msg = ""
    if stayOnTop:
        msg = "forcing window to stay on top "
        runCommand(f"wmctrl -ir {pid} -b add,above")
    return msg


def __getKeys(dict):
    list = []
    for key in dict.keys():
        list.append(key)
    return list


def __buildPositionWithExtents(pid, position):
    newPos = position
    lrtb = runWithOutput(f'xprop -id {pid} | grep GTK_FRAME_EXTENTS | cut -f2 -d "="')
    if lrtb != "":
        l, r, t, b = map(int, lrtb.strip().split(", "))
        print(f"GTK extents: left={l}, right={r}, top={t}, bottom={b}")
        x = position.x + PANEL_W
        y = position.y + PANEL_H
        w = position.width + l + r
        h = position.height + t + b
        newPos = Position(x, y, w, h)
    else:
        lrtb = runWithOutput(f'xprop -id {pid} | grep NET_FRAME_EXTENTS | cut -f2 -d "="')
        if lrtb != "":
            l, r, t, b = map(int, lrtb.strip().split(", "))
            print(f"NET extents: left={l}, right={r}, top={t}, bottom={b}")
            x = position.x + PANEL_W
            y = position.y + PANEL_H
            w = position.width  # - l - r
            h = position.height - t - b
            newPos = Position(x, y, w, h)

    print(f"base position: {position.log()}")
    print(f"adjusted position: {newPos.log()}")

    return newPos


def __getPID(title, type=None):
    cOpt = f'grep "{type}" | ' if type != None else ""
    pid = runWithOutput(f'wmctrl -lpGx | {cOpt}grep "{title}" | cut -f1 -d " "')

    if "\n" in pid:
        cm = f" with type '{type}'" if type != None else ""
        print(f"Found multiple windows matching '{title}'{cm}")
        print("If this is not your desired outcome, rerun with --list and make your title and type more restrictive")
        pid = pid.split("\n")

    if pid == "":
        cm = f" with type '{type}'" if type != None else ""
        print(f"Could not locate window titled '{title}'{cm}")
        pid = []

    if not isinstance(pid, list):
        pid = [pid]

    return pid


def usage():
    print(f"Usage:\n\t{sys.argv[0]} --help [prints this mess] --list [prints out current screen config]")

    print(" ~/.screenrc will be loaded")
    print("snapWindow can be ")
    print(f":NAME: Named window positions {__getKeys(snapPositions)}")
    print("")
    print("when specifying window sizes MAX_W is maximum width and MAX_H is maximum height")


if __name__ == "__main__":
    #
    # Default Values
    #
    layoutConfig = os.environ["HOME"] + "/.screenrc"
    if not os.path.exists(layoutConfig):
        print(f"Screen layout configuration does not exist: {layoutConfig}")
        sys.exit(1)

    #
    # Setup the command line options parser
    #
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["list", "conf=", "help", "desks", "test"])

        for opt, arg in opts:
            if opt in ("--conf"):
                layoutConfig = arg
            elif opt in ("--help", "-h"):
                usage()
                sys.exit()
            elif opt in ("--list"):
                runCommand(f"wmctrl -lGpx")
                sys.exit()
            elif opt in ("--desks"):
                runCommand(f"wmctrl -d")
                sys.exit()
            elif opt in ("--test"):
                testOnly = True
    except getopt.GetoptError:
        print(f"Usage:\n\t{sys.argv[0]} --conf <path to config> [default: ~/.screenlayout\n")
        sys.exit(2)

    #
    # execute the user configured calls
    #
    builtins.exec(open(layoutConfig).read())